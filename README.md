# ~~gentoo-dotfiles~~

~~Settings for my laptop~~

I am back to Arch.
```
 INSERT  gunix  ~  screenfetch 
                   -`                 
                  .o+`                 gunix@archtop
                 `ooo/                 OS: Arch Linux 
                `+oooo:                Kernel: x86_64 Linux 4.18.16-arch1-1-ARCH
               `+oooooo:               Uptime: 38m
               -+oooooo+:              Packages: 619
             `/:-:++oooo+:             Shell: zsh 5.6.2
            `/++++/+++++++:            Resolution: 2560x1440
           `/++++++++++++++:           WM: i3
          `/+++ooooooooooooo/`         GTK Theme: Adapta-Nokto-Eta [GTK2/3]
         ./ooosssso++osssssso+`        Icon Theme: Papirus
        .oossssso-````/ossssss+`       Font: Zekton 11
       -osssssso.      :ssssssso.      CPU: Intel Core i7-8550U @ 8x 4GHz [41.0°C]
      :osssssss/        osssso+++.     GPU: intel
     /ossssssss/        +ssssooo/-     RAM: 1595MiB / 31701MiB
   `/ossssso+/:-        -:/+osssso+-  
  `+sso+:-`                 `.-/+oso: 
 `++:.                           `-/+/
 .`                                 `/
```

Sorry. Gentoo is great, but I don't have time to take care of it.